import { Tag } from '../../models'

class TagController {
  static get = (req, res) => Tag.findAll().then(
    (data) => res.status(200).json(data),
  ).catch(
    (e) => console.log(e),
  )

  static create = (req, res) => {
    const { name } = req.body

    return Tag.create({
      name,
    }).then(
      (data) => res.status(201).json({ ...data.dataValues }),
    ).catch(
      (e) => console.log(e),
    )
  }

  static update = (req, res) => {
    const { id } = req.params

    return Tag.findOne({
      where: { id },
    }).then(
      (tag) => {
        if (!tag) return res.status(404).json({ message: 'Not found' })

        const { name } = req.body

        return tag.update(
          { name },
          { returning: true },
        ).then(
          (updated) => res.status(200).json({ ...updated.dataValues }),
        )
      },
    ).catch(
      (e) => console.log(e),
    )
  }

  static delete = (req, res) => {
    const { id } = req.params

    return Tag.destroy({
      where: { id },
    }).then(
      (data) => {
        if (!data) return res.status(404).json({ message: 'Not found' })

        return res.status(200).json({ message: 'Deleted' })
      },
    ).catch(
      (e) => console.log(e),
    )
  }
}

export default TagController

import { body } from 'express-validator'

class PostValidator {
  static createData = () => [
    body('id').exists().isUUID(),
    body('title').exists().isString(),
    body('body').exists().isString(),
    body('authorId').exists().isString(),
  ]

  static create = () => [
    body('id').exists().isInt({ gt: 0 }),
    body('title').exists().isString(),
    body('author').exists().isString(),
  ]
}

export default PostValidator
